﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
   public class GameManager : MonoSingleton<GameManager>
   {
      public static GameManager Instance { get; private set; }


      [SerializeField] private RectTransform dialog;
      [SerializeField] private PlayerSpaceship playerSpaceship;
      [SerializeField] private EnemySpaceship enemySpaceship;
      [SerializeField] private AudioSource audioSource;
      public event Action OnRestarted;
      [SerializeField] private int playerSpaceshipHp;
      [SerializeField] private int playerSpaceshipMoveSpeed;
      [SerializeField] private int enemySpaceshipHp;
      [SerializeField] private int enemySpaceshipMoveSpeed;

      [SerializeField] private Bullet bullet;

      private int currentScene = 1;
      private bool changeScene = false;


      private void Awake()
      {
         Debug.Assert(dialog != null, "dialog cannot be null");
         Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
         Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
         Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
         Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
         Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
         Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

         if (Instance == null)
         {
            Instance = this;
         }

         DontDestroyOnLoad(this);

      }

      public void OnStartButtonClicked()
      {
         dialog.gameObject.SetActive(false);
         StartGame();
         SoundManager.Instance.PlayBGM();
      }

      public void StartGame()
      {
         ScoreManager.Instance.Init(this);
         SpawnPlayerSpaceship();
         SpawnEnemySpaceship();
      }

      private void SpawnPlayerSpaceship()
      {
         var spaceship = Instantiate(playerSpaceship);
         spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed, bullet);
         spaceship.OnExploded += OnPlayerSpaceshipExploded;
      }

      private void OnPlayerSpaceshipExploded()
      {
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Die);
         UIManager.Instance.OnRestartGame();
      }

      private void SpawnEnemySpaceship()
      {
         var spaceship = Instantiate(enemySpaceship);
         spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed, bullet);
         spaceship.OnExploded += OnEnemySpaceshipExploded;
      }

      private void OnEnemySpaceshipExploded()
      {
         ScoreManager.Instance.SetScore(1);
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Die);
         UIManager.Instance.OnRestartGame();
         currentScene += 1;
         SceneManager.LoadSceneAsync("Level2");
         UIManager.Instance.OnGoNextLevel();

      }

      private void Restart()
      {
         dialog.gameObject.SetActive(true);
         OnRestarted?.Invoke();
      }

      public void GoNextLevel()
      {
         StartGame();
         SoundManager.Instance.PlayBGMLevel2();
         UIManager.Instance.OnCloseGoNextLevelDialog();
      }


   }
}
