﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoSingleton<SoundManager>
{
    [SerializeField] private  SoundClip[] soundClips;
    [SerializeField] private AudioSource audioSource;

    public static SoundManager Instance { get; private set; }

    public  enum Sound 
    {
        BGM,
        Fire,
        Die,
        BGMLEVEL2
    }

    [System.Serializable]

    public struct SoundClip 
    {
        public Sound Sound;
        public AudioClip AudioClip;
    }

    public void Play(AudioSource audioSource, Sound sound)
    {
        Debug.Assert(audioSource !=null,  "audioSource cannot be null");

        audioSource.clip = GetAudioClip(sound);
        audioSource.Play();

    }

    private AudioClip GetAudioClip(Sound sound)
    {
        foreach (var soundClip in soundClips)
        {
            if(soundClip.Sound == sound)
            {
                return soundClip.AudioClip;
            }
        }

        Debug.Assert(false, $"Cannot find sound {sound}");
        return null;
    }

    public void PlayBGM() 
    {
        audioSource.loop = true;
        Play(audioSource, Sound.BGM);
    }

    public void PlayBGMLevel2() 
    {
        audioSource.loop = true;
        Play(audioSource, Sound.BGMLEVEL2);
    }

    private void Awake() 
    {
        Debug.Assert( audioSource != null, "audioSource cannot be null");
        Debug.Assert( soundClips != null && soundClips.Length != 0, "sound clips need to be setup");

        if (Instance == null)
        {
            Instance = this;   
        }

        DontDestroyOnLoad(this);
    }
}

