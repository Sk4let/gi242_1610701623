﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Manager
{
   public class UIManager : MonoSingleton<UIManager>
   {
      public static UIManager Instance { get; private set; }

      [SerializeField] private GameObject resultDialog;
      [SerializeField] private TextMeshProUGUI scoreText;
      [SerializeField] private GameObject gonextPanel;

      private void Awake()
      {
         if (Instance == null)
         {
            Instance = this;
         }

         DontDestroyOnLoad(this);
      }

      public void OnRestartGame()
      {
         resultDialog.gameObject.SetActive(true);
         scoreText.text = "High Score : " + ScoreManager.Instance.Score.ToString();
      }

      public void OnGoNextLevel()
      {
         gonextPanel.gameObject.SetActive(true);
      }

      public void OnCloseGoNextLevelDialog()
      {
         gonextPanel.gameObject.SetActive(false);
      }

   }
}
