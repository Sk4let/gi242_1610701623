using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Spaceship
{
   public class PlayerSpaceship : Basespaceship, IDamagable
   {

      public event Action OnExploded;


      private void Awake()
      {
         Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
         Debug.Assert(gunPosition != null, "gunPosition cannot be null");

      }

      public void Init(int hp, float speed,Bullet bullet)
      {
         base.Init(hp, speed, defaultBullet);
      }

      public override void Fire()
      {
         var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
         bullet.Init();
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
      }

      public override void Fire2()
      {
         var bullet2 = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
         bullet2.Init();
         SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
      }

      public void TakeHit(int damage)
      {
         Hp -= damage;

         if (Hp > 0)
         {
            return;
         }
         
         Explode();
      }

      public void Explode()
      {
         Debug.Assert(Hp <= 0, "HP is more than zero");
         Destroy(gameObject);
         OnExploded?.Invoke();
      }

   }
}